#!/usr/bin/env python3
"""A tool that generates a chart comparing the gears of several bikes."""
import argparse
import itertools
import math
from dataclasses import dataclass

import pygame

UNITS = {
    "m": 1,
    "dm": 0.1,
    "cm": 0.01,
    "mm": 0.001,
    "ft": 0.3048,
    "in": 0.0254,
}

BACK_COLOUR = (255, 255, 255)
LINE_COLOUR = (119, 119, 119)
BLOB_COLOUR = (0, 0, 0)
LABEL_COLOUR = (0, 0, 0)


def length(value, unit="mm"):
    return UNITS[unit] * value


def gear_ratio(front, rear):
    return front / rear


def development(front, rear, full_radius):
    return front / rear * full_radius * math.tau


def gear_inches(*args):
    return development(*args) / math.pi / UNITS["in"]


def gain_ratio(front, rear, full_radius, crank):
    return front / rear * full_radius / crank


@dataclass
class Bike:
    name: str
    front: list
    rear: list
    full_radius: float = length(700, "mm")
    crank: float = length(172.5, "mm")

    @classmethod
    def from_data(cls, data):
        if "full_radius" not in data:
            result = 0
            if "wheel_radius" in data:
                result += data["wheel_radius"]
            elif "wheel_diameter" in data:
                result += data["wheel_diameter"] / 2

            if "tyre_thickness" in data:
                result += data["tyre_thickness"]

            if "full_diameter" in data:
                result = data["full_diameter"] / 2

            elif "full_circumference" in data:
                result = data["full_circumference"] / math.tau

        for used_key in (
            "wheel_radius",
            "wheel_diameter",
            "tyre_thickness",
            "full_diameter",
            "full_circumference",
        ):
            if used_key in data:
                del data[used_key]
        data["full_radius"] = result
        data["front"] = sorted(data["front"])
        data["rear"] = sorted(data["rear"], reverse=True)
        return cls(**data)

    def get_gain_ratios(self):
        ratios = [
            (
                gain_ratio(f, r, self.full_radius, self.crank),
                (self.front.index(f) + 1, self.rear.index(r) + 1),
            )
            for f, r in itertools.product(self.front, self.rear)
        ]
        ratios.sort()
        return ratios


def load_bikes(file_obj):
    """Generate a list of bikes by parsing a file."""
    bikes = []
    current_data = {}
    for row in file_obj:
        row = row.strip()
        if row[0] == "#":
            if current_data:
                bikes.append(Bike.from_data(current_data))
            current_data = {"name": row[2:]}
        else:
            variable, value = row.split(":")
            variable = variable.strip()
            if "," in value:
                value = [float(v.strip()) for v in value.split(",") if v.strip()]
            else:
                value = length(
                    *(
                        int(v)
                        if set(v).issubset("0123456789")
                        else float(v)
                        if set(v).issubset("0123456789.")
                        else v
                        for v in value.split(" ")
                        if v
                    )
                )
            current_data[variable] = value
    if current_data:
        bikes.append(Bike.from_data(current_data))

    return bikes


def to_x(width, value, max_value):
    return math.log(value) / math.log(max_value) * width * 0.95 + 0.02


@dataclass
class Drawer:
    size: tuple[int, int]
    aa: int

    @property
    def draw_size(self):
        return (self.size[0] * self.aa, self.size[1] * self.aa)

    @property
    def label_font(self):
        return pygame.font.Font(None, self.p(4))

    @property
    def name_font(self):
        return pygame.font.Font(None, self.p(2))

    @property
    def radius(self):
        return self.p(1)

    def scaled(self):
        return (
            self.image
            if self.aa == 1
            else pygame.transform.smoothscale(self.image, self.size)
        )

    def p(self, x, /) -> int:
        """Convert from percent of total height to pixels."""
        return round(self.draw_size[1] * x / 100)

    def draw(self, bikes):
        self.image = pygame.Surface(self.draw_size)
        if BACK_COLOUR != (0, 0, 0):
            self.image.fill(BACK_COLOUR)
        max_ratio = max(max(b.get_gain_ratios()) for b in bikes)[0]
        self.draw_background(max_ratio)

        # Draw bikes
        for index, bike in enumerate(bikes):
            y = self.draw_size[1] * (index + 1) / (len(bikes) + 1)
            self.draw_bike(bike, y, max_ratio)
        return self.scaled()

    def draw_bike(self, bike, y, max_ratio):
        position = 1
        forced_pos = True
        for ratio, gears in bike.get_gain_ratios():
            front_diff = gears[0] - len(bike.front) / 2 - 0.5
            rear_diff = gears[1] - len(bike.rear) / 2 - 0.5
            this_y = y + self.draw_size[1] * 0.03 * front_diff
            x = to_x(self.draw_size[0], ratio, max_ratio)
            drawpos = (round(x), round(this_y))
            is_gear_bad = (len(bike.rear) - 1) * 0.25 <= abs(rear_diff) * abs(
                front_diff
            ) and rear_diff * front_diff < 0
            pygame.draw.circle(
                self.image,
                BLOB_COLOUR,
                drawpos,
                self.radius,
                is_gear_bad * self.p(0.2),
            )

            label = self.name_font.render(
                f"{gears[0]}–{gears[1]}" if len(bike.front) > 1 else str(gears[1]),
                True,
                LABEL_COLOUR,
            )
            if gears[0] > len(bike.front) / 2 + 0.5:
                position = 1
                forced_pos = True
            elif gears[0] < len(bike.front) / 2 + 0.5:
                position = -1
                forced_pos = True
            else:
                if forced_pos:
                    position = -position
                forced_pos = False
            text_y = (
                this_y
                - label.get_height() / 2
                + position * (label.get_height() / 2 + self.radius * 1.1)
            )
            self.image.blit(label, (round(x - label.get_width() / 2), round(text_y)))
        label = self.label_font.render(bike.name, True, LABEL_COLOUR)
        self.image.blit(label, (self.p(2), round(y - label.get_height() / 2)))

    def draw_background(self, max_ratio):
        for i in range(1, 200):
            i /= 10
            x = to_x(self.draw_size[0], i, max_ratio)
            thickness = self.p(0.03 if i % 0.5 else 0.1 if i % 1 else 0.3)
            if x - thickness // 2 > self.draw_size[0]:
                break
            elif x + thickness // 2 < 0:
                continue

            pygame.draw.line(
                self.image,
                LINE_COLOUR,
                (round(x), 0),
                (round(x), self.draw_size[1]),
                thickness,
            )
            if not i % 1:
                label = self.label_font.render(str(round(i)), True, LINE_COLOUR)
                self.image.blit(
                    label,
                    (
                        round(x - label.get_width() - self.draw_size[1] * 0.003),
                        self.p(1),
                    ),
                )


def show(size, aa, bikes, image_file=None):
    pygame.init()
    pygame.display.set_caption("Gears")
    image = Drawer(size, aa).draw(bikes)
    S = pygame.display.set_mode(size, pygame.RESIZABLE)
    S.blit(image, (0, 0))
    if image_file is not None:
        pygame.image.save(image, image_file.name)
    clock = pygame.time.Clock()
    while True:
        for event in pygame.event.get():
            if (
                event.type == pygame.QUIT
                or event.type == pygame.KEYDOWN
                and event.key == pygame.K_ESCAPE
            ):
                return
            elif event.type == pygame.VIDEORESIZE:
                size = S.get_size()
                S.blit(Drawer(size, aa).draw(bikes), (0, 0))
        pygame.display.flip()
        clock.tick(30)


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "bikes", type=argparse.FileType("r"), help="bike gears data file"
    )
    parser.add_argument("-x", "--width", type=int, help="window width", default=1280)
    parser.add_argument("-y", "--height", type=int, help="window height", default=720)
    parser.add_argument(
        "-s",
        "--supersampling",
        type=int,
        help="amount of supersamping antialiasing to use",
        default=8,
    )
    parser.add_argument(
        "-o",
        "--output-image",
        type=argparse.FileType("wb"),
        help="image file to output to",
    )
    args = parser.parse_args()
    bikes = load_bikes(args.bikes)
    show(
        (args.width, args.height),
        args.supersampling,
        bikes,
        image_file=args.output_image,
    )


if __name__ == "__main__":
    main()
